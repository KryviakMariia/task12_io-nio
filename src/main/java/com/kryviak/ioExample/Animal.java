package com.kryviak.ioExample;


import java.io.Serializable;

public class Animal implements Serializable {
    private String name;
    private int age;
    private int weight;

    Animal(String name, int age, int weight){
        this.name = name;
        this.weight = weight;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight (int weight) {
        this.weight = weight;
    }

    public String toString() {
        return "Name of animal:" + this.name + " "
                + "Age: " + this.age + " " + "Weight: "
                + this.weight;
    }
}
