package com.kryviak.ioExample;

import java.io.*;
import java.util.Scanner;

import org.apache.logging.log4j.*;

public class IOStreamExample implements Serializable {

    private static Logger logger = LogManager.getLogger(IOStreamExample.class);

    private static Animal animal = new Animal("Cat", 1, 4);
    private static Animal animal1 = new Animal("Dog", 5, 10);
    private static Animal animal2 = new Animal("Elephant", 10, 355);

    void setAnimalInFileWithBuf() {
        try (
                OutputStream outputStream = new FileOutputStream("file2.txt");
                BufferedOutputStream buf = new BufferedOutputStream(outputStream);
                DataOutputStream out = new DataOutputStream(buf)
        ) {
            out.writeUTF(animal.toString());
            out.writeUTF(animal1.toString());
            out.writeUTF(animal2.toString());

        } catch (IOException e) {
            logger.error("File not found");
        }
    }

    void setAnimalInFileWithoutBuf() {
        try {
            OutputStream output = new FileOutputStream("file.txt");
            output.write(animal.toString().getBytes());
            output.write(animal1.toString().getBytes());
            output.write(animal2.toString().getBytes());
            output.close();

        } catch (IOException e) {
            logger.error("File not found");
        }
    }

     void getAnimalWithBuf() {
        String nameAnimal;
        logger.info("Read data with buffer:");

        try (
                InputStream is = new FileInputStream("file2.txt");
                BufferedInputStream bis = new BufferedInputStream(is);
                DataInputStream in = new DataInputStream(bis)) {
            while (in.available() > 0) {
                nameAnimal = in.readUTF();
                logger.info(nameAnimal);
            }
        } catch (IOException e) {
            logger.error("File not found");
        }
    }

     void getAnimalWithChar() {
        logger.info("Get date with char:");
        byte[] bFile = readBytesFromFile("file.txt");
        for (int i = 0; i < bFile.length; i++) {
            logger.info((char) bFile[i]);
        }
    }

    private byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            logger.error("File not found");
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    logger.error("File not found");
                }
            }
        }
        return bytesArray;
    }

     void setAnimalWithSerializable() {
        try {
            FileOutputStream fos = new FileOutputStream("file3.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(animal);
            oos.writeObject(animal1);
            oos.close();
        } catch (IOException e) {
            logger.error("File not found");
        }
    }

     void getAnimalWithSerializable() {
        try {
            FileInputStream fis = new FileInputStream("file3.txt");
            ObjectInputStream oin = new ObjectInputStream(fis);
            Animal ts = (Animal) oin.readObject();
            Animal ts2 = (Animal) oin.readObject();
            logger.info(ts.toString());
            logger.info(ts2.toString());
            oin.close();
        } catch (IOException e) {
            logger.error("File not found");
        } catch (ClassNotFoundException e) {
            logger.error("Class not found");
        }
    }

     void getBigDataWithByte() {
        logger.info("Start read big data with byte");
        byte[] bFile = readBytesFromFile("somevideo.mp4");
        for (int i = 0; i < bFile.length; i++) {
            System.out.print("");
        }
        logger.info("Finish read big data");
    }

     void getBigDataWithBuf() {
        logger.info("Start read big date with buffer");
        InputStream is = null;
        try {
            is = new FileInputStream("somevideo.mp4");
        } catch (FileNotFoundException e) {
            logger.error("File not found");
        }
        BufferedInputStream bis = new BufferedInputStream(is, 10000);
        try {
            int data = bis.read();

            while (data != -1) {
                data = bis.read();

            }
        } catch (IOException e) {
            logger.error("File not found");
        }
        logger.info("Finish read big date");
    }

     void getAllNameOfPackages() {
        Scanner scanner = new Scanner(System.in);
        String fileName = getFileNameFromFolder("D:\\projects");
        logger.info("Please enter package name:");
        String str = scanner.nextLine();
        if (fileName.contains(str)) {
            logger.info("Package: '" + str + "' is present");
        } else {
            logger.info("Package: '" + str + "' not found");
        }
    }

    private static String getFileNameFromFolder(String folderName) {
        File folder = new File(folderName);
        File[] listOfFiles = folder.listFiles();
        StringBuilder sb = new StringBuilder();
        for (File listOfFile : listOfFiles) {
            logger.info(listOfFile.getName());
            sb.append(listOfFile.getName()).append(" ");
        }
        return sb.toString();

    }
}
