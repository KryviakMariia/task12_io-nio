package com.kryviak.nioExample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class NIOStreamExample {

    private static Logger logger = LogManager.getLogger(NIOStreamExample.class);

    void writeAndReadBigDate() {
        logger.info("Start copy data");
        try {
            byte[] bFile = Files.readAllBytes(new File("somevideo.mp4").toPath());

            Path path = Paths.get("test2.mp4");
            Files.write(path, bFile);

            for (int i = 0; i < bFile.length; i++) {
                System.out.print("");
            }
            logger.info("Complete");
        } catch (
                IOException e) {
            logger.error("File not found");
        }
    }

    void copyFile() {
        Path oldFile = Paths.get("file.txt");
        Path newFile = Paths.get("newFile.txt");
        try (OutputStream os = new FileOutputStream(newFile.toFile())) {

            Files.copy(oldFile, os);

        } catch (IOException ex) {
            logger.error("File not found");
        }

    }
}