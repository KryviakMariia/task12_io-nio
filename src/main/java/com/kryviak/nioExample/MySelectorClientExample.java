package com.kryviak.nioExample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;


class MySelectorClientExample {
    static final int ONE_KB = 1024;
    private static Logger logger = LogManager.getLogger(MySelectorClientExample.class);

    Socket client;
    InetSocketAddress addr;

    MySelectorClientExample(String ip, int port) {

        client = new Socket();
        addr = new InetSocketAddress(ip, port);

    }


    void connect() {
        try {
            client.connect(addr);
        } catch (IOException e) {
            logger.error("Socket is closed");

        }
    }

    String read() {

        byte[] temp = new byte[ONE_KB];

        int bytesRead = 0;

        try (InputStream input = client.getInputStream()) {
            bytesRead = input.read(temp);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String a = null;
        try {
            a = new String(temp, 0, bytesRead, "ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return a;
    }

    void write(String data) {
        byte[] temp = new byte[ONE_KB];

        try (OutputStream out = client.getOutputStream()) {
            out.write(data.getBytes());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void close() {
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
