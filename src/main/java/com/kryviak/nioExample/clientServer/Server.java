package com.kryviak.nioExample.clientServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static Logger logger = LogManager.getLogger(Server.class);
    private static Socket clientSocket; //сокет для спілкування
    private static ServerSocket server; // серверсокет
    private static BufferedReader in;  // потік для читання з сокету
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                server = new ServerSocket(4004);
                logger.info("Server was run");
                clientSocket = server.accept(); // accept() буде чекати поки хтось підключиться
                try {
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                    String word = in.readLine(); // чекаєм поки клієнт щось напише
                    logger.info(word);
                    out.write("Hello I'm server. Your message is : " + word + "\n");
                    out.flush();

                } finally {
                    clientSocket.close();
                    in.close();
                    out.close();
                }
            } finally {
                System.out.println("Server is closed");
                server.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }
}