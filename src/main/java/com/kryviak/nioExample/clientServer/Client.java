package com.kryviak.nioExample.clientServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class Client {
    private static Logger logger = LogManager.getLogger(Client.class);

    private static Socket clientSocket; //сокет для спілкування
    private static BufferedReader reader; // буфер який зчитує з консолі, те, що ми ввели
    private static BufferedReader in;
    private static BufferedWriter out;
    public static void main(String[] args) {

        try {
            try {
                clientSocket = new Socket("localhost", 4004); // запит на підключення
                reader = new BufferedReader(new InputStreamReader(System.in));
                // читати повідомлення з серверу
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                // запис повідомлення
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                logger.info("Hello. Please write your message:");

                String word = reader.readLine(); // чекаєм поки клієнт щось напише в консоль
                out.write(word + "\n"); // відправляєм повідомлення на сервер
                out.flush();
                String serverWord = in.readLine(); // чекаєм що скаже сервер
                logger.info(serverWord); // виводимо в консоль
            } finally {
                logger.info("Client was closed");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }


}
