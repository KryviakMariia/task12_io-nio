package com.kryviak.nioExample;

import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;


class MySelectorServerExample {
    static final int ONE_KB = 1024;

    ServerSocket socket;
    Socket listener;

    MySelectorServerExample(int port) {
        try {
            socket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    void connect() {
        try {
            listener = socket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String read() {

        byte[] temp = new byte[ONE_KB];

        int bytesRead = 0;

        try (InputStream input = listener.getInputStream()) {
            bytesRead = input.read(temp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String a = null;
        try {
            a = new String(temp, 0, bytesRead, "ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return a;
    }

    void write(String data) {
        byte[] temp = new byte[ONE_KB];

        try (OutputStream out = listener.getOutputStream()) {
            out.write(data.getBytes());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}