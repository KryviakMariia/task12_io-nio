package com.kryviak.nioExample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Console;

public class MainNIO {
    static final int PORT = 9999;
    static final String ADDRESS = "localhost";
    private static Logger logger = LogManager.getLogger(MainNIO.class);

    public static void main(String[] args) {
        NIOStreamExample nioStreamExample = new NIOStreamExample();
        MySelectorClientExample mySelectorClientExample = new MySelectorClientExample(ADDRESS, PORT );
        MySelectorServerExample mySelectorServerExample = new MySelectorServerExample( PORT);


        MySelectorClientExample mySelectorClientExample1 = new MySelectorClientExample(ADDRESS, PORT );
        MySelectorServerExample mySelectorServerExample1 = new MySelectorServerExample( 8080);


//        nioStreamExample.copyFile();
//        nioStreamExample.writeAndReadBigDate();

        mySelectorClientExample.connect();
        mySelectorServerExample.connect();
        mySelectorClientExample.write("Hello");
        logger.info(mySelectorServerExample.read());
        mySelectorClientExample.close();
        mySelectorServerExample.close();

    }
}
